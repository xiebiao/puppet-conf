class web_server::apache inherits web_server {
    case $operatingsystem {
        centos, redhat: { $apache = "httpd" }
        debian, ubuntu: { $apache = "apache2" }
        default: { 
                   fail("Unrecognized operating system for webserver")
                 }
    }
    
#   notify {'showvar':
#        message => "$apache"
#    }

    Notify['info'] {
        message => "This is child class"
    }
}
