class web_server::nginx inherits web_server {

    case $operatingsystem {
        centos, redhat: { $nginx = "nginx" }
        debian, ubuntu: { $nginx = "nginx" }
        default: { 
                   fail("Unrecognized operating system for webserver")
                 }
    }
    
}
