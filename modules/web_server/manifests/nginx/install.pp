class web_server::nginx::install inherits web_server::nginx {
    package {$nginx:
        ensure => latest
    }
}
