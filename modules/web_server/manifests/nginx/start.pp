
include web_server::nginx::install

class web_server::nginx::start inherits web_server::nginx {
    service {$nginx:
        ensure => true
    }
}
