#--
#-- Test on Ubuntu
#--
class develop_tools {
    # Install SCM tools
    case $operatingsystem {
        centos, redhat: { 
            $svn = "subversion"
            $git = "git-core"

        }
        debian, ubuntu: { 
            $svn = "subversion"
            $git = "git"
        }
        default: { 
            fail("Unrecognized operating system for SCM")
        }
    }

    package {'svn':
        name   => $svn,
        ensure => latest
    }
    
    package {'git':
        name   => $git,
        ensure => latest
    }
    
    package {
        "vim", 
        "meld", /* 代码合并查看工具 */
        "curl",
        "devhelp" /* 用于查看python，gtk等文档 */
    }
    
    package {   /* C/C++开发环境  */
        "build-essential",
        "autoconf",
        "automake",
        "flex",
        "bison",
        "manpages-dev",
        "binutils-doc",
        "cpp-doc",
        "gcc-doc",
        "glibc-doc",
        "stl-manual"
    }
    
    package {   
        "wireshark", /* 网络调试工具 */
        "netcat-openbsd" /* 网络工具中的神器:netcat */
    }   
}
