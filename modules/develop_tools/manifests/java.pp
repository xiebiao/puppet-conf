#
# eclipse ; http://www.eclipse.org 
# ant : http://ant.apache.org
# maven : http://maven.apache.org
# tattletale : http://www.jboss.org/tattletale
# pmd : http://pmd.sourceforge.net/
# findbugs : http://findbugs.sourceforge.net/
# checkstyle : http://checkstyle.sourceforge.net/
# cobertura : http://cobertura.sourceforge.net/
# tcpmon : http://ws.apache.org/commons/tcpmon/
#
class develop::java($user="xiaog") {
    
    $ant_version    = "1.8.2"
    $maven_version  = "3.0.3"

    Exec {
        path =>['/usr/bin']
    
    }

#    exec {'download_ant':
#        command =>
#            "wget -X /home/$user/dev/ http://www.apache.org/dist/ant/binaries/apache-ant-$ant_version-bin.tar.gz"
#,
#        logoutput => true
#    }
    
#    exec {'download_maven':
#        command => "wget -P /home/$user/dev/ http://www.apache.org/dist//maven/binaries/apache-maven-$maven_version-bin.tar.gz"
#   ,
#       logoutput => true
#    }
    
}
