# My personal work platform
class software {
    package {
        [

        /* SCM */    
        "git",
        "gitg",
        "subversion",    
        "bzr",
        "vim",
        "meld",
        "curl",
        "terminator",
        "devhelp",

        /* c/c++ */
        "build-essential",
        "autoconf",
        "automake",
        "flex",
        "bison",
        "manpages-dev",
        "binutils-doc",
        "cpp-doc",
        "gcc-doc",
        "glibc-doc",
        "stl-manual",
        
        /* gnome */
        "gnome-core-devel",
        "pkg-config",
        "libglib2.0-doc",
        "glade",

        /* debug *
        "wireshark",

        /* commons */
        "chromium-browser",
        "stardict",
        "tomboy",
        "pinfo"/* 比man更好用的帮助软件 */
        ]:

        ensure => latest;
    }

}
